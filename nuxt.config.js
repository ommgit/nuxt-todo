
module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'blog',
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {property: 'og:title', content: 'Nuxt.js title'},
      {property: 'og:description', content: 'Nuxt.js project'},
      {property: 'og:image', content: 'https://admin.allpasal.com/uploads/general-setting/chpy15a2gn_1616731492.png'},
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
      {rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900'},
      {rel: 'stylesheet', href: 'https://cdn.jsdelivr.net/npm/@mdi/font@latest/css/materialdesignicons.min.css'},
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: {color: '#3B8070'},
  /*
  ** Build configuration
  */

  plugins: [
    '@/plugins/mixins/pluralize',
    '@/plugins/scrollTo',
    {
      src: '@/plugins/vueSelect',
      ssr: false
    },

  ],

  buildModules:[
    ['@nuxtjs/vuetify',{

      theme: {
        themes: {
          light: {
            primary: '#003574',
            gray: '#757575',
            black: '#333',
            mediumBlack: '#212121',
            darkBlue: '#003574',
            darkOrange: '#FF5C00',
            logInIconBackground: '#e9e6e6',
            logInTextFieldBorder: '#003574',
            activeColor: '#f36f21',
            cardColor: '#ececec',
            headerPrimaryColor: '#e1e7ef',
            primaryHeaderColor: '#e1e7ef',
            headerSecondaryColor: '#F5F5F5',
          },
        },
      },

    }]

  ],

  build: {
    /*
    ** Run ESLint on save
    */

    build: {
      vendor: [
        'axios',
        'vuetify'
      ]
    },

    extend(config, {isDev, isClient}) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },
  },

  css: [
    '@/assets/css/app.css',
    '@/assets/scss/app.scss'

  ]

}

